using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lvv1
{
    class Note
    {
        private String text;
        private String author;
        private int priority;
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int Priority
        {
            get{ return this.priority; }
            set { this.priority = value; }
        }

        public String getText()
        {
            return this.text;
        }
        public String getAuthor()
        {
            return this.author;
        }
        public int getPriority()
        {
            return this.priority;
        }
        public void setText(String text)
        {
            this.text = text;
        }
        public void setPriority(int priority)
        {
            this.priority = priority;
        }
        public Note()
        {
            this.text = "matko";
            this.author = "Matko Mihalj";
            this.priority = 5;
        }
        public Note(string text)
        {
            this.text = text;
            this.author = "Matko Mihalj";
            this.priority = 5;
        }
        public Note(string text, int priority)
        {
            this.text = text;
            this.author = "Matko Mihalj";
            this.priority = priority;
        }
        public override string ToString()
        {
            return this.text + this.author + this.priority;
        }
    }
}
