using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lvv1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note;
            note = new Note();
            Console.WriteLine(note.getAuthor());
            Console.WriteLine(note.getText());
            Note Secondnote;
            Secondnote = new Note("Mali Princ", 3);
            Console.WriteLine(Secondnote.getText());
            Console.WriteLine(Secondnote.getAuthor());
            Note Thirdnote = new Note("Mali Princ");
            Console.WriteLine(Thirdnote.getText());
            Console.WriteLine(Thirdnote.getAuthor());
            Timenote timelynote = new Timenote();
            Console.WriteLine(timelynote.ToString());
            Console.ReadLine();
            
        }
    }
}
