
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lvv1
{
    class Time: Note
    {
        private DateTime time;
        public Time()
        {
            this.time = DateTime.Now;
        }
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public Time(string text, string author, DateTime time, int priority)
        {
            this.time = time;

        }
        public override string ToString()
        {
            return base.ToString()+this.time;
        }
    }
}
